(ns main
  (:require
   [compiler.analyze :as a]
   [compiler.parser :as p]
   [compiler.emitter.emit-instr :as emit]
   [compiler.instructions :as i]
   [clojure.string :as string]))

(defn into-bytecode
  [instrs]
  (reduce #(if (i/label? %2)
             (string/replace %1 #"\t$" (str (second %2) "\t"))
             (str %1 (string/replace (reduce (fn [acc arg] (str acc " " arg ",")) (first %2) (rest %2)) #",$" "") "\n\t")) "\t" instrs))

(defn compile
  [{:keys [file output]}]
  (let [content (slurp file)
        ast (p/parse-ast content)
        analyzed (a/analyze ast)
        instrs (emit/emit-instr analyzed)
        bytecode (into-bytecode instrs)]
    (spit output bytecode)))

;; run with clj -M:main test.txt
;; run with clj -M:main -o test.bc test.txt

(defn -main
  [& args]
  (loop [options {:output "a.out"
                  :file nil}
         [& args] args]
    (case (count args)
      0 (compile options)
      1 (compile (conj options [:file (first args)]))
      (let [[arg1 arg2 & args] args]
        (case arg1
          "-o" (recur (conj options [:output arg2]) args)
          (recur (conj options [:file arg1]) (cons arg2 args)))))))