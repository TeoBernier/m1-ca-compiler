(ns compiler.instructions)

(defn const
  [n] [(list 'CONST n)])

(defn prim
  [f] [(list 'PRIM f)])

(defn label
  [l] [(list 'LABEL l)])

(defn branch
  [l] [(list 'BRANCH l)])

(defn branch-if-not
  [l] [(list 'BRANCHIFNOT l)])

(defn push
  [] ['(PUSH)])

(defn pop
  [] ['(POP)])

(defn assign
  [n] [(list 'ASSIGN n)])

(defn acc
  [i] [(list 'ACC i)])

(defn env-acc
  [i] [(list 'ENVACC i)])

(defn mk-block
  [n] [(list 'MAKEBLOCK n)])

(defn get-field
  [n] [(list 'GETFIELD n)])

(defn set-field
  [n] [(list 'SETFIELD n)])

(defn get-length
  [] ['(VECTLENGTH)])

(defn get-item
  [] ['(GETVECTITEM)])

(defn set-item
  [] ['(SETVECTITEM)])

(defn closure
  [l n] [(list 'CLOSURE l n)])

(defn apply
  [n] [(list 'APPLY n)])

(defn return
  [n] [(list 'RETURN n)])

(defn stop
  [] ['(STOP)])

(defn label?
  [instr] (and (seq instr) (= 'LABEL (first instr))))

(defmulti deref-label (fn [instr] (keyword (first instr))))

(defmethod deref-label :BRANCH
  [[sym label]]
  (list sym @label))

(defmethod deref-label :BRANCHIFNOT
  [[sym label]]
  (list sym @label))

(defmethod deref-label :LABEL
  [[sym label]]
  (list sym @label))

(defmethod deref-label :CLOSURE
  [[sym label n]]
  (list sym @label n))

(defmethod deref-label :default
  [instr]
  instr)