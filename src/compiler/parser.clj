(ns compiler.parser
  (:require [instaparse.core :as insta]))


(def rules
  "PROG = STAT-IF
   
   <STAT-IF> = BEGIN
             | LET-IF
             | IF
             | IF-ELSE
             | WHILE
             | SET
             | PRINT
   
   <STAT> = BEGIN
          | LET
          | IF-ELSE
          | WHILE
          | SET
          | PRINT

   BEGIN = <'begin'> SEP BLOC <'end'> SEP
   LET-IF = <'let'> SEP IDENT <'='> EXPR <'in'> SEP STAT-IF
   LET = <'let'> SEP IDENT <'='> EXPR <'in'> SEP STAT
   IF = <'if'> SEP EXPR <'then'> SEP STAT-IF
   IF-ELSE = <'if'> SEP EXPR <'then'> SEP STAT <'else'> SEP STAT-IF
   WHILE = <'while'> SEP EXPR <'do'> SEP BLOC <'done'> SEP
   SET = IDENT <':='> EXPR
   PRINT = 'print' SEP EXPR

   <BLOC> = STAT-IF
          | STAT-IF <';'> PAD? BLOC
   
   <EXPR> = ORD10
   
   <CONST> = NUM
           | TRUE
           | FALSE
          
   NUM = #'[0-9]+' SEP
   TRUE = <'true'> SEP
   FALSE = <'false'> SEP
   
   <IDENT> = !(RESERVED SEP) #'[_a-zA-Z][_a-zA-Z0-9]*' SEP
   
   RESERVED = 'if' | 'then' | 'else' | 'begin' | 'end' 
            | 'while' | 'do' | 'done' | 'let' | 'in'
            | 'ref' | 'print' | 'not' | 'true' | 'false'

   
   ORD10_EXPR = ORD20 ('&&' | '||') PAD? ORD10
   <ORD10> = ORD10_EXPR | ORD20

   ORD20_EXPR = ORD30 ('<=' | '>=' | '<' | '>' | '==' | '!=') PAD? ORD30
   <ORD20> = ORD20_EXPR | ORD30
        
   ORD30_EXPR = ORD30 ('+' | '-') PAD? ORD40
   <ORD30> = ORD30_EXPR | ORD40
        
   ORD40_EXPR = ORD40 ('*' | '/') PAD? ORD50
   <ORD40> = ORD40_EXPR | ORD50
        
   <ORD50> = REF | DEREF | ATOM

   REF = <'ref'> SEP ATOM
   DEREF = <'!'> SEP ATOM

   <ATOM> = <'('> PAD? EXPR <')'> PAD?
          | CONST
          | IDENT
   
   <PAD> = <#'[ \n\t]+'>
   <SEP> = & ( #'$'| ';' | ':' | '+' | '-' | '*' | '/' | '<'
         | '>' | '=' | '!' | '(' | ')' | '[' | ']') | <PAD>")

(def prog_parser
  (insta/parser rules :output-format :hiccup))

(defn create-if
  ([cond then]
   {:op :if, :cond cond, :then then
    :children [:cond :then]})
  ([cond then else]
   {:op :if, :cond cond, :then then, :else else
    :children [:cond :then :else]}))

(defn create-prog
  [& body]
  {:op :prog, :body (vec body)
   :children [:body]})

(defn create-begin
  [& body]
  {:op :begin, :body (vec body)
   :children [:body]})

(defn create-while
  [cond & body]
  {:op :while, :cond cond, :body (vec body)
   :children [:cond :body]})

(defn create-let
  [sym init & body]
  {:op :let, :sym (symbol sym), :init init, :body (vec body)
   :children [:sym :init :body]})

(defn create-set
  [sym value]
  {:op :set, :sym (symbol sym), :value value
   :children [:sym :value]})

(defn create-true
  [] true)

(defn create-false
  [] false)

(defn create-num
  [value] (read-string value))

(defn create-primitive
  ([function e]
   {:op :primitive, :function (symbol function), :args [e], :arity 1
    :children [:function :args]})
  ([e1 function e2]
   {:op :primitive, :function (symbol function), :args [e1 e2], :arity 2
    :children [:function :args]}))

(defn create-ref
  [value]
  {:op :ref :value value :children [:value]})

(defn create-deref
  [value]
  {:op :deref :value value :children [:value]})

(def transform-map
  {:PROG create-prog
   :IF create-if
   :IF-ELSE create-if
   :BEGIN create-begin
   :WHILE create-while
   :LET create-let
   :LET-IF create-let
   :SET create-set
   :TRUE create-true
   :FALSE create-false
   :NUM create-num
   :PRINT create-primitive
   :ORD10_EXPR create-primitive
   :ORD20_EXPR create-primitive
   :ORD30_EXPR create-primitive
   :ORD40_EXPR create-primitive
   :REF create-ref
   :DEREF create-deref})

(defn parse-ast [source]
  (insta/transform transform-map (prog_parser source)))


(parse-ast "if true then x:=3 + 2 *1 +3 +4 + !(ref 5) else print 4 ")