(ns compiler.emitter.emit-form
  (:require [compiler.ast :as a]))



(defmulti -emit-form #(or (:op %) %))

(defmethod -emit-form :if
  [node]
  (list* 'if (:cond node) 'then (:then node) (when (:else node) (list 'else (:else node)))))

(defmethod -emit-form :while
  [node]
  (list 'while (:cond node) 'do (:body node) 'done))

(defmethod -emit-form :begin
  [node]
  (list 'begin (:body node) 'end))

(defmethod -emit-form :let
  [node]
  (list 'let (:sym node) '= (:init node) 'in (:body node)))

(defmethod -emit-form :set
  [node]
  (list (:sym node) ':= (:value node)))

(defmethod -emit-form :primitive
  [node]
  (cons (:function node) (:args node)))

(defmethod -emit-form :ref
  [node]
  (list 'ref (:value node)))

(defmethod -emit-form :deref
  [node]
  (list '! (:value node)))

(defmethod -emit-form :prog
  [node]
  (:body node))

(defmethod -emit-form :default
  [node]
  node)


(defn emit-form [ast]
  (a/postwalk ast -emit-form))