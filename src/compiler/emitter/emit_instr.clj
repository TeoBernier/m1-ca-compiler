(ns compiler.emitter.emit-instr
  (:require [compiler.instructions :as i]))

(declare -emit-instr)



(defn compress-label
  [instrs]
  (if (< 2 (count instrs))
    (loop [[inst1 inst2 & instrs] instrs
           acc []]
      (let [[acc remaining]
            (if (and (i/label? inst1) (i/label? inst2))
              (do
                (swap! (second inst2) (fn [_] @(second inst1)))
                [(conj acc inst1) instrs])
              [(conj acc inst1) (cons inst2 instrs)])]
        (if (empty? instrs)
          (into acc remaining)
          (recur remaining acc))))
    instrs))



(defn remove-label
  [instrs]
  (mapv i/deref-label instrs))




(declare ^:dynamic label-counter)

(defn gen-label []
  (let [n (swap! label-counter inc)]
    (atom (symbol (str "L" n)))))



(defn env-empty
  [] '())

(def env-add cons)

(defn env-get
  [sym env]
  (and (seq env)
       (loop [[assoc-sym & remaining] env
              n 0]
         (if (= sym assoc-sym)
           n
           (if (empty? remaining)
             false
             (recur remaining (inc n)))))))




(defn instrs
  [& args]
  (loop [[arg & args] args
         acc []]
    (let
     [vec (if (vector? arg)
            (into acc (apply instrs arg))
            (conj acc arg))]
      (if (empty? args)
        vec
        (recur args vec)))))


(defn -emit-body
  [body env]
  (instrs (mapv #(-emit-instr % env) body)))






(defmulti -emit-instr (fn [node _] (or (:op node) node)))

(defmethod -emit-instr :default
  [atom env]
  (cond
    (number? atom) (i/const atom)
    (boolean? atom) (i/const (if atom 1 0))
    (symbol? atom) (let [rank (env-get env atom)]
                     (if rank
                       (i/acc rank)
                       (throw (Exception. (str "Unknown variable : " atom)))))
    :else (throw (Exception. (str "Not a valid node : " atom)))))

(defn -emit-prim
  ([function env arg] 
   (instrs
    (-emit-instr arg env)
    (i/prim function)))
  ([function env arg1 arg2]
   (instrs
    (-emit-instr arg2 env)
    (i/push)
    (-emit-instr arg1 (env-add nil env))
    (i/prim function))))

(defmethod -emit-instr :primitive
  [{:keys [function args]} env]
  (apply -emit-prim (list* function env args)))

(defmethod -emit-instr :begin
  [{:keys [body]} env]
  (-emit-body body env))

(defmethod -emit-instr :prog
  [{:keys [body]} env]
  (instrs (-emit-body body env) (i/stop)))


(defmethod -emit-instr :if
  [{:keys [cond then else]} env]
  (if else
    (let [label-else (gen-label)
         label-end (gen-label)]
     (instrs
      (-emit-instr cond env)
      (i/branch-if-not label-else)
      (-emit-instr then env)
      (i/branch label-end)
      (i/label label-else)
      (-emit-instr else env)
      (i/label label-end)))
    (let [label-end (gen-label)]
     (instrs
      (-emit-instr cond env)
      (i/branch-if-not label-end)
      (-emit-instr then env)
      (i/branch label-end)))))


(defmethod -emit-instr :while
  [{:keys [cond body]} env]
  (let [label-body (gen-label)
        label-cond (gen-label)]
    (instrs
     (i/branch label-cond)
     (i/label label-body)
     (-emit-body body env)
     (i/label label-cond)
     (-emit-instr cond env)
     (i/prim 'not)
     (i/branch-if-not label-body))))


(defmethod -emit-instr :let
  [{:keys [sym init body]} env]
  (instrs
   (-emit-instr init env)
   (i/push)
   (-emit-body body (env-add sym env))
   (i/pop)))


(defmethod -emit-instr :set
  [{:keys [sym value]} env]
  (instrs
   (-emit-instr value env)
   (i/assign (env-get sym env))))


(defmethod -emit-instr :ref
  [{:keys [value]} env]
  (instrs
   (-emit-instr value env)
   (i/mk-block 1)))


(defmethod -emit-instr :deref
  [{:keys [value]} env]
  (instrs
   (-emit-instr value env)
   (i/get-field 0)))

(defn emit-instr
  ([node] (emit-instr node (env-empty)))
  ([node env]
   (binding [label-counter (atom -1)]
     (remove-label (compress-label (-emit-instr node env))))))


