(ns compiler.analyze
  (:require
   [compiler.parser :as p]
   [compiler.ast :refer [postwalk prewalk walk]]
   [compiler.passes :refer [compress-operations compress-begin]]
   [compiler.emitter.emit-form :refer [emit-form]]
   [compiler.emitter.emit-instr :refer [emit-instr]]))

(defn print-op
  [node]
  (if (:op node)
    (print "\n" (:op node) " ")
    (print node " "))
  node)




(def analyze
  (comp))

(emit-form (analyze (p/parse-ast "if true then x:=3 + 2 *1 +3 +4 + ! (ref 5) else x:=4 ")))

(emit-instr (analyze (p/parse-ast "while true do if true then print 1 else print 2 done")))

(emit-instr (analyze (p/parse-ast "begin print 1 ; print 2 ; begin print 3 end end")))

(emit-form (analyze (p/parse-ast "print 1 - 2 - 3 - 4 - 5")))

(emit-instr (analyze (p/parse-ast "print 1 - 2 - 3 - 4 - 5")))

