(ns compiler.ast)

(defn map-children
  [f node]
  (into node (map
              #(let [child (% node)]
                 (if (vector? child)
                   [% (mapv f child)]
                   [% (f child)]))
              (:children node))))

(defn walk
  [ast pre post]
  (let [pre-ast (pre ast)
        mid-ast (map-children #(walk % pre post) pre-ast)
        post-ast (post mid-ast)]
    post-ast))

(defn prewalk
  [ast pre]
  (walk ast pre identity))

(defn postwalk
  [ast post]
  (walk ast identity post))